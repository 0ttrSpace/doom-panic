# Doom Panic 

This is a theme created on as a fork from the Panic Mode theme by [Bernardo C.D.A. Vasconcelos](https://github.com/bcdavasconcelos/Obsidian-Panic_Mode) who did an awesome job. I wanted to use this as a base because 
I felt it matched what I was trying to do the best and gave me a great jumpping of point to create a DoomOne (based on the DoomEmacs DoomOne theme) based obsidian theme. 

The main framework for my Obsidian Vault is based off [Bryan Jenkin's](https://www.bryanjenks.dev/obsidian-only) Obsidian vault. 

I hope you enjoy this theme and if you have ideas on how to make it look more "DoomOne" or something like that then please make a PR. 

Ryan '0ttrSpace' Kohalmy

![Screen Cap](https://gitlab.com/0ttrSpace/doom-panic/-/raw/main/doom-panic-cap.png)
